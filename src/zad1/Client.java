package zad1;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class Client implements Runnable {

    ServerSocket serverSocketTranslation;
    static Integer selfPort = 13000;

    String translatedValue;

    @Override
    public void run() {
        try{
            serverSocketTranslation = new ServerSocket(selfPort);
        } catch (IOException exception) {
            System.out.println("Cannot create socket");
        }

        while(true) {
            try {
                Socket connectionSocket = serverSocketTranslation.accept();
                BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));

                translatedValue = inFromClient.readLine();
                System.out.println(translatedValue);
            } catch (Exception ex) {
                System.out.println("Unable to get translated message");
            }
        }
    }

    public static String sendTranslationRequest(String word, String languageKey) throws Exception {
        Socket clientSocket = new Socket("localhost", 12000);
        DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
        BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        outToServer.writeBytes(languageKey + "," + word + "," + selfPort + "\n");
        String response = inFromServer.readLine();
        System.out.println(response);

        clientSocket.close();
        return response;
    }
}
