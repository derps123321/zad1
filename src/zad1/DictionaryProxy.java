package zad1;

import java.io.*;
import java.net.*;
import java.util.*;

public class DictionaryProxy implements Runnable {

	ServerSocket serverSocketProxy;

    int englishServerSocket = 20000;
    int frenchServerSocket = 30000;
    int russianServerSocket = 40000;

    Map<String, Integer> eachDictionarySocket;
	
	public DictionaryProxy() {
        eachDictionarySocket = new HashMap<>();
        eachDictionarySocket.put("en", englishServerSocket);
        eachDictionarySocket.put("fr", frenchServerSocket);
        eachDictionarySocket.put("ru", russianServerSocket);
    }
	
	@Override
	public void run() {
        try{
        	serverSocketProxy = new ServerSocket(12000);
        } catch (IOException exception) {
            System.out.println("Cannot create socket");
        }
		
        while(true) {
            try {
                Socket connectionSocket = serverSocketProxy.accept();
                BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
                DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());

                String clientSentence = inFromClient.readLine();
                String[] responseTable = clientSentence.split(",");
                Integer clientPort = Integer.parseInt(responseTable[2]);
                String dictionaryKey = responseTable[1].toLowerCase();
                String dictionaryValue = responseTable[0];
                if (eachDictionarySocket.containsKey(dictionaryKey)) {
                    System.out.println("Dictionary found: " + dictionaryKey + ", Value: " + dictionaryValue + "\n");
                    sendTranslationRequest(dictionaryValue, dictionaryKey, clientPort);
                } else {
                    outToClient.writeBytes("Dictionary not found\n");
                }

            } catch (Exception ex) {
                System.out.println("Cannot establish connection.");
            }
        }
	}

	public void sendTranslationRequest(String value, String dictionaryKey, int clientPort) {
	    try {
	        int dictionarySocket = eachDictionarySocket.get(dictionaryKey);
            Socket clientSocket = new Socket("localhost", dictionarySocket);
            DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
            BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            outToServer.writeBytes(value.toLowerCase() + "," + clientPort + "\n");
            String response = inFromServer.readLine();
            System.out.println(response);
            //TODO: potencjalne rozwiazanie problemu gdy nie ma tlumaczenia
            clientSocket.close();
        } catch (Exception ex) {
            System.out.println("Cannot connect to dictionary" + dictionaryKey);
        }
    }

}
