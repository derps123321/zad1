package zad1;

import jdk.nashorn.internal.ir.IfNode;

import java.io.*;
import java.net.*;
import java.util.Map;

public abstract class DictionaryServer implements Runnable {

    ServerSocket dictionaryServerSocket;
    int socket;
    Map<String, String> dictionaryMap;

    @Override
    public void run() {
        try{
            dictionaryServerSocket = new ServerSocket(socket);
        } catch (IOException exception) {
            System.out.println("Cannot create socket");
        }

        while(true) {
            try {
                Socket connectionSocket = dictionaryServerSocket.accept();
                BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
                DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());

                String clientSentence = inFromClient.readLine();
                String[] responseTable = clientSentence.split(",");
                String input = responseTable[0];
                Integer outputSocket = Integer.parseInt(responseTable[1]);
                String output = dictionaryMap.get(input);

                if (output != null) {
                    System.out.println("Translated from: " + input + "\n");
                    outToClient.writeBytes("Success translating");
                    sendTranslatedWord(output, outputSocket);
                } else {
                    outToClient.writeBytes("Dictionary not found\n");
                }

            } catch (Exception ex) {
                System.out.println("Cannot establish connection.");
            }
        }
    }

    public void sendTranslatedWord(String output, int clientPort) {
        try {
            Socket clientSocket = new Socket("localhost", clientPort);
            DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());

            outToServer.writeBytes(output + "\n");

            clientSocket.close();
        } catch (Exception ex) {
            System.out.println("Cannot establish connection to client");
        }
    }
}
