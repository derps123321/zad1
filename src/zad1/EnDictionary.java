package zad1;

import java.util.Hashtable;

public class EnDictionary extends DictionaryServer {

    private static EnDictionary instance;

    private EnDictionary(){
        this.socket = 20000;
        fillDictionaryMap();
    }

    public static EnDictionary getInstance(){
        if(instance == null){
            instance = new EnDictionary();
        }
        return instance;
    }

    public void fillDictionaryMap() {
        dictionaryMap = new Hashtable<>();
        dictionaryMap.put("kot", "cat");
    }
}
