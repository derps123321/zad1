package zad1;

import java.util.Hashtable;

public class FrDictionary extends DictionaryServer {

    private static FrDictionary instance;

    private FrDictionary(){
        this.socket = 30000;
        fillDictionaryMap();
    }

    public static FrDictionary getInstance(){
        if(instance == null){
            instance = new FrDictionary();
        }
        return instance;
    }

    public void fillDictionaryMap() {
        dictionaryMap = new Hashtable<>();
        dictionaryMap.put("kot", "chat");
    }

}
