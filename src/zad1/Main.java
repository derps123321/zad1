package zad1;

import java.io.*;
import java.net.*;

public class Main {

	public static void main(String[] args) {

		String dictionaryKey = "en";
		String dictionaryValue = "kot";

		DictionaryProxy proxyDictionary = new DictionaryProxy();
		Thread proxyThread = new Thread(proxyDictionary);
		EnDictionary enDictionary = EnDictionary.getInstance();
		Thread enThread = new Thread(enDictionary);
		FrDictionary frDictionary = FrDictionary.getInstance();
		Thread frThread = new Thread(frDictionary);
		RuDictionary ruDictionary = RuDictionary.getInstance();
		Thread ruThread = new Thread(ruDictionary);

		proxyThread.start();
		enThread.start();
		frThread.start();
		ruThread.start();

		Client client = new Client();
		Thread clientThread = new Thread(client);

		clientThread.start();
		try {
			Client.sendTranslationRequest("PL", dictionaryValue);
			System.out.println("dupa1");
		} catch (Exception ex) {
			System.out.println("nie udało się");
		}
		try {
			Client.sendTranslationRequest(dictionaryKey, dictionaryValue);
		} catch (Exception ex) {
			System.out.println("nie udało się");
		}
	}

}
