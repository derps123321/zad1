package zad1;

import java.util.Hashtable;

public class RuDictionary extends DictionaryServer {

    private static RuDictionary instance;

    private RuDictionary(){
        this.socket = 40000;
        fillDictionaryMap();
    }

    public static RuDictionary getInstance(){
        if(instance == null){
            instance = new RuDictionary();
        }
        return instance;
    }

    public void fillDictionaryMap() {
        dictionaryMap = new Hashtable<>();
        dictionaryMap.put("kot", "кот");
    }
}
